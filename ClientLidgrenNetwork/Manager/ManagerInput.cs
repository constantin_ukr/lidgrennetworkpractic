﻿using System;

namespace ClientLidgrenNetwork.Manager
{
    class ManagerInput
    {
        private readonly ManagerNetwork _managerNetwork;
        private readonly ConsoleKey[] _inputControl;
        public ManagerInput(ManagerNetwork managerNetwork)
        {
            _managerNetwork = managerNetwork;
            _inputControl = new ConsoleKey[]
                {ConsoleKey.A, ConsoleKey.D, ConsoleKey.W, ConsoleKey.S};
        }

        public void Update(double gameTime)
        {
            int index = new Random().Next(0, _inputControl.Length);
            ConsoleKey state = _inputControl[index];
            CheckKeyState(state);
        }

        private void CheckKeyState(ConsoleKey state)
        {
            _managerNetwork.SendInput(state);
        }
    }
}
