﻿using System;
using System.Collections.Generic;
using System.Linq;
using Library;
using Lidgren.Network;

namespace ClientLidgrenNetwork.Manager
{
    public class ManagerNetwork
    {
        private NetClient _client;
        public List<Player> Players { get; set; }
        public bool Active { get; private set; }
        public string Username { get; set; }

        public ManagerNetwork()
        {
           Players = new List<Player>();
        }

        public bool Start()
        {
            Random random = new Random();
            _client = new NetClient(new NetPeerConfiguration("networkGame"));
            _client.Start();
            Username = string.Format("name_{0}", random.Next(0, 100));
            var outmsg = _client.CreateMessage();
            outmsg.Write((byte)PacketType.Login);
            outmsg.Write(Username);
            _client.Connect("localhost", 14241, outmsg);
            Console.WriteLine("connection status: {0}",_client.ConnectionStatus);
            Console.WriteLine("port: {0}",_client.Port);
            return EsablishInfo();
        }

        private bool EsablishInfo()
        {
            DateTime time = DateTime.Now;
            NetIncomingMessage inc;

            while (true)
            {
                if (DateTime.Now.Subtract(time).Seconds > 5)
                {
                    Console.WriteLine("Time more 5 sec!");
                    return false;
                }

                if ((inc = _client.ReadMessage()) == null)
                {
                    continue;
                }

                switch (inc.MessageType)
                {
                    case NetIncomingMessageType.Data:
                        byte data = inc.ReadByte();
                        byte login = (byte) PacketType.Login;
                        if (data == login)
                        {
                            Console.WriteLine("Connected!");

                            Active = inc.ReadBoolean();
                            if (Active)
                            {
                                Console.ForegroundColor = ConsoleColor.Green;
                                ReceiveAllPlayers(inc);

                                return true;
                            }
                            else
                            {
                                return false;
                            }
                        }
                        else
                        {
                            return false;
                        }
                    case NetIncomingMessageType.StatusChanged:
                        NetConnectionStatus status = (NetConnectionStatus)inc.ReadByte();

                        if (status == NetConnectionStatus.Connected)
                        {
                            Console.WriteLine("Status: Connected");
                        }
                        if (status == NetConnectionStatus.Disconnected)
                        {
                            Console.WriteLine("Status: Disconnected");
                        }
                        Console.WriteLine(inc.ReadString());
                        break;
                    case NetIncomingMessageType.VerboseDebugMessage:
                        Console.WriteLine("VerboseDebugMessage");
                        Console.WriteLine(inc.ReadString());
                        break;
                }
            }
        }

        public void Update()
        {
            NetIncomingMessage inc;
            while ((inc = _client.ReadMessage()) != null)
            {
                if (inc.MessageType != NetIncomingMessageType.Data)
                {
                    continue;
                }

                PacketType packetType = (PacketType)inc.ReadByte();
                switch (packetType)
                {
                    case PacketType.PlayerPosition:
                        ReadPlayer(inc);
                        break;
                    case PacketType.AllPlayers:
                        ReceiveAllPlayers(inc);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        /*private void ReceivePlayerPosition(NetIncomingMessage inc)
        {
            Player player = new Player();
            inc.ReadAllProperties(player);
            Player oldPlayer = Players.FirstOrDefault(p => p.Name == player.Name);
            if (oldPlayer != null)
            {
                oldPlayer.XPosition = player.XPosition;
                oldPlayer.YPosition = player.YPosition;
            }
            else
            {
                Players.Add(player);
            }
        }*/

        private void ReceiveAllPlayers(NetIncomingMessage inc)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            int count = inc.ReadInt32();
            try
            {
                if (count <= 0)
                {
                    return;
                }

                for (int i = 0; i < count; i++)
                {
                    ReadPlayer(inc);
                }
            }
            catch (Exception e)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(e);
                throw;
            }

            Console.ForegroundColor = ConsoleColor.Gray;
        }

        private void ReadPlayer(NetIncomingMessage inc)
        {
            Player player = new Player();
            inc.ReadAllProperties(player);
            Console.WriteLine("ReadAllProperties");


            if (Players.Count > 0 && Players.All(p => p.Name == player.Name))
            {
                Player oldPlayer = Players.FirstOrDefault(p => p.Name == player.Name);
                if (oldPlayer != null)
                {
                    //oldPlayer.XPosition = player.XPosition;
                    //oldPlayer.YPosition = player.YPosition;
                    Console.WriteLine("OtherPlayers.All");
                    //Console.WriteLine("Other Player info: Name: {0}, Position:({1}, {2})",
                    //            oldPlayer.Name, oldPlayer.XPosition, oldPlayer.YPosition);
                }
            }
            else
            {
                Console.WriteLine("OtherPlayers.Add");
                Players.Add(player);
                //Console.WriteLine("Other Player info: Name: {0}, Position:({1}, {2})",
                //            player.Name, player.XPosition, player.YPosition);
            }
        }

        public void SendInput(ConsoleKey key)
        {
            NetOutgoingMessage outmsg = _client.CreateMessage();
            outmsg.Write((byte)PacketType.Input);
            outmsg.Write((byte)key);
            outmsg.Write(Username);
            _client.SendMessage(outmsg, NetDeliveryMethod.ReliableOrdered);
        }
    }
}
