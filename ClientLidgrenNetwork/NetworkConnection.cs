﻿using System;
using Library;
using Lidgren.Network;

namespace ClientLidgrenNetwork
{
    public class NetworkConnection
    {
        private NetClient _client;

        public bool Start()
        {
            var loginInformation = new NetworkLoginInformation() { Name = "RandomName" };
            _client = new NetClient(new NetPeerConfiguration("networkGame"));
            _client.Start();
            var outmsg = _client.CreateMessage();
            outmsg.Write((byte)PacketType.Login);
            outmsg.WriteAllProperties(loginInformation);
            _client.Connect("localhost", 14241, outmsg);
            return EsablishInfo();
        }

        private bool EsablishInfo()
        {
            DateTime time = DateTime.Now;
            NetIncomingMessage inc;

            while (true)
            {
                if (DateTime.Now.Subtract(time).Seconds > 5)
                {
                    return false;
                }

                if ((inc = _client.ReadMessage()) == null)
                {
                    continue;
                }

                switch (inc.MessageType)
                {
                        case NetIncomingMessageType.Data:
                        byte data = inc.ReadByte();

                        if (data == (byte) PacketType.Login)
                        {
                            Console.WriteLine("Connected!");
                            return inc.ReadBoolean();
                            //bool accepted = inc.ReadBoolean();
                            //if (accepted)
                            //{
                            //    return true;
                            //}
                            //else
                            //{
                            //    return false;
                            //}
                        }

                        break;
                }
            }
        }
    }
}
