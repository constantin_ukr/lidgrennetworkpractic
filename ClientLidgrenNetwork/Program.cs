﻿using System;
using System.Threading;
using ClientLidgrenNetwork.Manager;

namespace ClientLidgrenNetwork
{
    class Program
    {
        private static ManagerNetwork _managerNetwork;
        private static ManagerInput _managerInput;
        static void Main()
        {
            _managerNetwork = new ManagerNetwork();
            if (_managerNetwork.Start())
            {
                Console.ForegroundColor = ConsoleColor.Green;
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
            }
            _managerInput = new ManagerInput(_managerNetwork);
            //Thread.Sleep(100);
            while (true)
            {
                if (_managerNetwork.Active)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    _managerNetwork.Update();
                    _managerInput.Update(0);
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("_managerNetwork.Active false!");
                    break;
                }
                Thread.Sleep(300);
            }
            

            Console.ReadKey();
        }
    }
}
