﻿namespace Library
{
    public enum PacketType: byte
    {
        Login,
        PlayerData,
        AllPlayers,
        Input
    }
}
