﻿using System;
using System.Collections.Generic;
using System.Linq;
using Library;
using Lidgren.Network;
using System.Threading;
using UnityEngine;

namespace TestLidgrenNetwork
{
    class Server
    {
        //private List<Player> _players;
        private NetPeerConfiguration _config;
        private NetServer _server;

        public Server()
        {
            //_players = new List<Player>();
            _config = new NetPeerConfiguration("networkGame") { Port = 14241 };
            //_config.EnableMessageType(NetIncomingMessageType.ConnectionApproval);
            //_config.EnableMessageType(NetIncomingMessageType.StatusChanged);
            //_config.EnableMessageType(NetIncomingMessageType.Data);

            _server = new NetServer(_config);
        }

        public void Run()
        {
            Console.WriteLine("Server started...");
            _server.Start();

            while (true)
            {
                NetIncomingMessage inc;
                if ((inc = _server.ReadMessage()) == null)
                {
                    continue;
                }

                switch (inc.MessageType)
                {
                    case NetIncomingMessageType.StatusChanged:
                        foreach (NetConnection conn in _server.Connections)
                        {
                            string str = string.Format("{0} from {1} [{2}]",
                                NetUtility.ToHexString(conn.RemoteUniqueIdentifier), conn.RemoteEndPoint, conn.Status);
                            Console.WriteLine(str);
                        }
                        break;
                    case NetIncomingMessageType.ConnectionApproval:
                        ConnectionApproval(inc);
                        break;
                    case NetIncomingMessageType.Data:
                        Data(inc);
                        break;
                    case NetIncomingMessageType.WarningMessage:
                        Console.WriteLine("WarningMessage");
                        break;
                    case NetIncomingMessageType.DebugMessage:
                        Console.WriteLine("DebugMessage");
                        Console.WriteLine(inc.ReadString());
                        break;
                    default:
                        Console.WriteLine("Other params");
                        break;
                        //throw new ArgumentOutOfRangeException();
                }
            }
        }

        private void Data(NetIncomingMessage inc)
        {
            Console.WriteLine("Data");
            MessageType messageType = (MessageType)inc.ReadByte();
            string name = inc.ReadString();

            switch (messageType)
            {
                case MessageType.Login:
                    Console.WriteLine("Logined user: {0}", name);
                    break;
                case MessageType.PlayerData:
                    Vector3 pos = inc.ReadVector3();
                    Quaternion rot = inc.ReadQuaternion();
                    Console.WriteLine("Player user: {0}\nPosition: ({1},{2},{3})\nRotation: ({4},{5},{6})",
                        name, pos.x, pos.y, pos.z, rot.x, rot.y, rot.z);
                    break;
                case MessageType.Dissconnect:
                    Console.WriteLine("Disconnected user: {0}", name);
                    break;
            }
        }

        private void ConnectionApproval(NetIncomingMessage inc)
        {
            Console.WriteLine("New connection...");

            Console.WriteLine("..connection accpeted.");

            inc.SenderConnection.Approve();

            NetOutgoingMessage outmsg = _server.CreateMessage();
            outmsg.Write((byte)MessageType.Login);
            outmsg.Write(true);
            //outmsg.Write(_players.Count);
            //for (int i = 0; i < _players.Count; i++)
            //{
            //    Console.WriteLine("Send player name: {0}", _players[i].Name);
            //    outmsg.WriteAllProperties(_players[i]);
            //}

            Thread.Sleep(50);
            Console.WriteLine(inc.SenderConnection.RemoteEndPoint.Address + ":" + inc.SenderConnection.RemoteEndPoint.Port);
            _server.SendMessage(outmsg, inc.SenderConnection, NetDeliveryMethod.ReliableOrdered, 0);
            Thread.Sleep(50);

            //}
            //else
            //{
            //    inc.SenderConnection.Deny("Didn't send correct information.");
            //}
        }


    }
}
